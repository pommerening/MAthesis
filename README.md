# Master Thesis Supplementary Material

The files shared here represent additional material relating to "Revisiting Sphendonoid balance weights: What can morphology tell us about standards?", which was written by Jonathan Pommerening as an MSc thesis in Archaeological Science at the University of Leiden.

By sharing both my data and methods I aim to contribute in improving the state of open science in the field of balance weight research. I hope that such initiatives can be beneficial for future research while also encouraging increased archaeological participation on this topic.


## File Overview

For better understanding I will briefly give an overview of the files I have shared here.

If you are interested in looking at the whole thesis send me an email at jonathan.pommerening8atgmail.com

**ThesisWeights.csv**

For my thesis I made use of 661 sphendonoid weights from 7 different sites in Western Asia during the  Bronze Age. In this file I collected information for each weight.

**ManualDigStatChar.R**

This file shows how the sphendonoid weights were digitised (see Section 3.2 of thesis).
Furthermore, the statistical characteristics, used to analyse the digitised weights (see Section 3.3 of thesis) are then applied to the digitised weights.

**DigitalWeights.csv**

Here, the x- and y-coordiantes are stored, which are required to carry out the digitisation in **ManualDigStatChar.R**.

**CQAFinal.R**

This script can be used to carry out Kendall's Cosine Quantogram Analysis. This analysis is used in balance weight research to identify past weight units based on the mass of a group of balance weights. 

## License
Feel free to redistribute, modify and adapt the original work. For further questions and additional material don't hesitate to contact me: jonathan.pommerening8atgmail.com
